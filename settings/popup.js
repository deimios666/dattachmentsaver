const defaultSettings = {
  "fileNamePattern": '%a (%b) %f%c.%e'
  //"deleteFile": false
}
const loadSettings = async () => {
  let db = await browser.storage.local.get('settings');
  let settings;
  if (db.settings === undefined) {
    settings = defaultSettings;
    await browser.storage.local.set({ settings });
  } else {
    settings = db.settings;
  }
  for (const key of Object.keys(settings)) {
    const elem = document.getElementById(key);
    if (elem.type === 'checkbox') {
      document.getElementById(key).checked = settings[key] === "true" || settings[key] === true;
    } else {
      document.getElementById(key).value = settings[key];
    }
  }
}

const saveSettings = async () => {
  const settings = {}
  for (const key of Object.keys(defaultSettings)) {
    const elem = document.getElementById(key);
    if (elem.type === 'checkbox') {
      settings[key] = document.getElementById(key).checked === "true" || document.getElementById(key).checked === true;
    }
    else {
      settings[key] = document.getElementById(key).value;
    }

  }
  await browser.storage.local.set({ settings });
}

const saveDefaultSettings = async () => {
  await browser.storage.local.clear();
  await loadSettings();
}

document.addEventListener("DOMContentLoaded", loadSettings);
document.getElementById('save').addEventListener('click', saveSettings);
document.getElementById('reset').addEventListener('click', saveDefaultSettings);
