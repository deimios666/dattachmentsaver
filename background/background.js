async function load() {

  //Load JSZip
  import("./jszip.min.js");

  //Create context menu structure
  let menu_id = await messenger.menus.create({
    title: "Attachement Saver",
    contexts: [
      "message_list"
    ],
  });

  const senderRegex = /([^<]*)\s*<(.*)>|([^@]+@[^@]+)/g;

  const defaultSettings = {
    "fileNamePattern": '%a (%b) %f%c.%e',
    //"deleteFile": false
  }

  const zeroPad = (num, places) => String(num).padStart(places, '0')

  // Register a listener for the menus.onClicked event.
  await messenger.menus.onClicked.addListener(async (info, tab) => {
    if (info.menuItemId == menu_id) {
      let settings;
      const db = await browser.storage.local.get('settings');
      if (db.settings === undefined) {
        settings = defaultSettings;
        await browser.storage.local.set({ settings });
      } else {
        settings = db.settings;
      }
      const { fileNamePattern, deleteFile } = settings;
      const zip = JSZip();
      //This tracks duplicate names
      const namesMap = new Map();
      const messages = (await browser.mailTabs.getSelectedMessages()).messages;
      for (const message of messages) {
        const attachments = await browser.messages.listAttachments(message.id);
        const regexResult = [...message.author.matchAll(senderRegex)];
        const matches = regexResult[0];
        let authorName = matches[1] ?? '';
        let authorEmail = matches[3] ?? matches[2];
        const valueMap = new Map();
        valueMap.set("%a", authorName);
        valueMap.set("%b", authorEmail);
        valueMap.set("%s", message.subject);
        valueMap.set("%Y", message.date.getFullYear());
        valueMap.set("%M", zeroPad(message.date.getMonth() + 1, 2));
        valueMap.set("%D", zeroPad(message.date.getDate(), 2));
        valueMap.set("%H", zeroPad(message.date.getHours(), 2));
        valueMap.set("%m", zeroPad(message.date.getMinutes(), 2));
        valueMap.set("%S", zeroPad(message.date.getSeconds(), 2));
        for (const attachment of attachments) {
          const file = await browser.messages.getAttachmentFile(message.id, attachment.partName);
          const fileArray = file.name.split('.')
          const fileExt = fileArray.pop();
          const fileOrigName = fileArray.join('.');
          valueMap.set("%f", fileOrigName);
          valueMap.set("%e", fileExt);

          //Replace all the values in the pattern
          let fileName = fileNamePattern;
          for (const [key, value] of valueMap) {
            fileName = fileName.replaceAll(key, value);
          }
          //If the name is already in the map, increment the duplication counter otherwise clear it
          if (namesMap.has(fileName)) {
            namesMap.set(fileName, namesMap.get(fileName) + 1);
            fileName = fileName.replaceAll("%c", namesMap.get(fileName));
            fileName = fileName.replaceAll("%u", "_".repeat(namesMap.get(fileName)));
          } else {
            namesMap.set(fileName, 0);
            fileName = fileName.replaceAll("%c", "");
            fileName = fileName.replaceAll("%u", "");
          }
          //invalid characters
          fileName = fileName.replaceAll(/[/\\?%*:|]/g, "-");
          //"
          fileName = fileName.replaceAll(/["]/g, "'");
          //<
          fileName = fileName.replaceAll(/[<]/g, "(");
          //>
          fileName = fileName.replaceAll(/[>]/g, ")");

          zip.file(fileName, await file.arrayBuffer());
          if (deleteFile) {
            try {
              await browser.messages.deleteAttachmentFile(message.id, attachment.partName);
            } catch (e) {
              console.log("Failed to delete attachment", attachment.partName, "with error", e);
            }
          }
        }
      }
      const zipcontent = await zip.generateAsync({ type: "blob" });
      const url = URL.createObjectURL(zipcontent);
      await browser.downloads.download({ url: url, filename: `attachments${new Date().getTime()}.zip`, conflictAction: 'uniquify' });
      //The ObjectURL is tied to the DOM so we need to clean it up
      //Cleanup runs one minute after the download is started
      const cleanup = setTimeout(() => URL.revokeObjectURL(url), 1000 * 60);
    }
  });
}

// Load the background script when the extension loads
document.addEventListener("DOMContentLoaded", load);
